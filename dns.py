import socket
import construct as st

flags = st.BitStruct("Flags",
    st.Flag("is_response"),
    st.Enum(st.Nibble("opcode"),
        query=0,
        inverse_query=1,
        status_request=2,
        _default_=st.Pass
    ),
    st.Flag("is_authoritative"),
    st.Flag("truncation"),
    st.Flag("recursion_desired"),
    st.Flag("recursion_available"),
    st.BitField("reserved", 3),
    st.Enum(st.Nibble("response_code"),
        ok=0,
        format_error=1,
        server_failure=2,
        name_error=3,
        not_implemented=4,
        refused=5,
        _default_=st.Pass
    )
)

Header = st.Struct("Header",
    st.UBInt16("id"),
    st.Embed(flags),
    st.UBInt16("question_count"),
    st.UBInt16("answer_count"),
    st.UBInt16("name_server_count"),
    st.UBInt16("additional_record_count")
)

def DomainName(field_name):
    return st.ExprAdapter(st.RepeatUntil(lambda object, context: len(object) == 0, st.PascalString(field_name)),
        encoder=lambda object, context: object.encode("utf-8").split(b"."),
        decoder=lambda object, context: b".".join(object).decode("utf-8")
    )
    
def Type(field_name):
    return st.Enum(st.UBInt16(field_name),
        A=1,
        NS=2,
        MD=3,
        MF=4,
        CNAME=5,
        SOA=6,
        MB=7,
        MG=8,
        MR=9,
        NULL=10,
        WKS=11,
        PTR=12,
        HINFO=13,
        MINFO=14,
        MX=15,
        TXT=16,
        AXFR=252,
        MAILB=253,
        MAILA=254,
        all=255,
        _default_=st.Pass
    )

def Class(field_name):
    return st.Enum(st.UBInt16(field_name),
        IN=1,
        CS=2,
        CH=3,
        HS=4,
        all=255,
        _default_=st.Pass
    )

Question = st.Struct("Question",
    DomainName("name"),
    Type("type"),
    Class("class_")
)

IPv4Address = st.ExprAdapter(st.Array(4, st.UBInt8("address")),
    encoder=lambda object, context: list(map(int, object.split("."))),
    decoder=lambda object, context: ".".join(str(byte) for byte in object),
)

ResourceRecord = st.Struct("ResourceRecord",
    DomainName("name"),
    Type("type"),
    Class("class_"),
    st.UBInt32("ttl"),
    st.UBInt16("data_length"),
    st.Switch("data", lambda context: context.type, {
        "A": IPv4Address,
    })
)

Message = st.Struct("Message",
    st.Embed(Header),
    st.Array(lambda context: context.question_count, st.Rename("questions", Question)),
    st.Array(lambda context: context.answer_count, st.Rename("answers", ResourceRecord))
)

class Server:
    """This class implements a simple DNS server that can only parse requests."""
    def __init__(self):
        self.socket = socket.socket(type=socket.SOCK_DGRAM)
        self.socket.bind(("127.0.0.1", 53))
    
    def mainloop(self):
        while True:
            message, sender = self.socket.recvfrom(512)
            self.process(message, sender)
            
    def process(self, message, sender):
        parsed = Message.parse(message)
        print("Request:")
        print(parsed)
        parsed.is_response = True
        parsed.is_authoritative = False
        parsed.recursion_available = True
        parsed.truncation = False
        parsed.answer_count = parsed.question_count
        parsed.answers = list(map(self.answer_for_question, parsed.questions))
        self.socket.sendto(Message.build(parsed), sender)
    
    def answer_for_question(self, question):
        return st.Container(name=question.name, type=question.type, class_=question.class_, ttl=0, data_length=4, data="255.255.255.255")

if __name__ == "__main__":
    try:
        Server().mainloop()
    except PermissionError:
        print("You must be root to run this program, since it needs to bind a socket on port 53.")